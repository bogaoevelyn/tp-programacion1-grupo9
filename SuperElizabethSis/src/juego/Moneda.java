package juego;

import java.awt.Color;
import entorno.Entorno;

public class Moneda {
	private int x;
	private int y;
	private double diametro;
	private String st;
	
	
	Moneda(int x, int y){
		this.x = x;
		this.y = y;
		this.diametro = 30;
		this.st = "$";
	}
	
	
	// M�todos
	
	public void dibujar(Entorno e) {
		e.dibujarCirculo(this.x, this.y, this.diametro, Color.YELLOW);
		e.dibujarCirculo(this.x, this.y, this.diametro-10, Color.ORANGE);
		e.cambiarFont(this.st, 15, Color.black);
		e.escribirTexto(this.st,this.x-2, this.y+6);
	}
	
	
	public void mover() {
		this.x = this.x - 1 ;
	}
	
	
	//getters necesarios para la clase Juego
	public int getY() {
		return y;
	}

	public int getX() {
		return x;
	}

	public double getDiametro() {
		return diametro;
	}
}

package juego;

import java.awt.Image;

import entorno.Entorno;

public class Princesa {
	// variables de instancia
	private int x;
	private int y;
	private int alto;
	private int ancho;
	private Image princesa;
	
	
	// constructor
	Princesa(Image Imagen, int x, int y, int alto, int ancho){
		this.princesa = Imagen;
		this.x = x;
		this.y = y;
		this.alto = alto;
		this.ancho = ancho;
		
	}
	
	// M�todos
	public void moverDer() {
		this.x = this.x + 1;
	}
	
	
	public void moverIzq() {
		this.x = this.x - 1;
	}
	
	
	public void dibujar(Entorno e) {
		e.dibujarImagen(this.princesa, this.x, this.y, 0, 1);
	}
	
	
	public void saltar() {
		this.y = this.y -20;
	}
	
	
	public void caer() {
		this.y = this.y +2;
	}
	
	
	public BolaDeFuego crearBolaFuego () {
		BolaDeFuego b = new BolaDeFuego(this.x + this.ancho/2 ,this.y,20);
		return b;
	}

	
    // Getters 
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getAlto() {
		return alto;
	}
	
	public int getAncho() {
		return ancho;
	}
}

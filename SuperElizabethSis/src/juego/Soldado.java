package juego;

import java.awt.Image;
import entorno.Entorno;

public class Soldado {
	// variables de instancia
	private double x;
	private int y;
	private int alto;
	private int ancho;
	private Image soldado;
	
	// Constructor
	Soldado(Image Imagen,double x, int y, int alto, int ancho){
		this.soldado = Imagen;
		this.x = x;
		this.y = y;
		this.alto = alto;
		this.ancho = ancho;
		}	

		
	//metodos
	public void moverIzquierda() {
		this.x = this.x - 1.5;
		}
		
	public void dibujar(Entorno e) {
		e.dibujarImagen(this.soldado, this.x, this.y, 0, 1);
	}
		
		
	// getters necesarios para la clase Juego
	public double getX() {
		return x;
	}

	public int getAncho() {
		return ancho;
	}

	public int getY() {
		return y;
	}

	public int getAlto() {
		return alto;
	}

}
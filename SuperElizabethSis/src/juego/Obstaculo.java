package juego;

import java.awt.Color;
import entorno.Entorno;

public class Obstaculo {
	// variables de Instancia
	private int x;
	private int y;
	private int alto;
	private int ancho;
	private Color color;
	
	
	// Definimos el constructor
	Obstaculo (int x, int y, int ancho, int alto,Color color){
		this.x = x;
		this.y = y;
		this.ancho = ancho;
		this.alto = alto;
		this.color = color;
	}
	
	
	// Metodos
	
	// Dibujamos el obstaculo
	public void dibujar (Entorno e) {
		e.dibujarRectangulo(this.x, this.y, this.ancho, this.alto, 0, this.color);
		e.dibujarRectangulo(this.x, this.y-this.alto*5/8, this.ancho+this.ancho/4, this.alto/4, 0, this.color);
		//this.y-this.alto*5/8 = this.y -this.alto/2 -this.alto/8 
		//(se le resta a mitad de la altura del rectangulo base y la mitad de la altura del superior, que su altura es 1/4
	}
	
	// creamos un metodo en el cual el obstaculo se mueva hacia la derecha, es su unico movimiento
	public void moverIzq () {
		this.x = this.x - 1;
	}

	
	// getters necesarios para la clase Juego
	public int getX() {
		return x;
	}

	public int getAncho() {
		return ancho;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public int getAlto() {
		return alto;
	}

}

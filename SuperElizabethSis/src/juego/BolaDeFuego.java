package juego;

import java.awt.Color;
import entorno.Entorno;

public class BolaDeFuego {
	// Variables de instancia
	private int x;
	private int y;
	private int diametro;


		
 	// Constructor
	BolaDeFuego(int x, int y, int diametro){
		this.x = x;
		this.y = y;
		this.diametro = diametro;
	}
	
	
	// M�todos
	
	public void dibujar(Entorno e) {
		e.dibujarCirculo(x, y, diametro, Color.RED);
	}
	
	
	public void mover() {
		this.x = this.x + 2 ;
	}
	
	
	// Getters necesarios para la clase Juego
	public int getY() {
		return y;
	}

	public int getX() {
		return x;
	}

	public int getDiametro() {
		return diametro;
	}
}
package juego;


import java.awt.Color;
import java.awt.Image;
import java.util.Random;
import java.util.LinkedList;
import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego
{
	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;
	
	// Variables de instancia
	
	private Princesa prin;
	private int contTick;
	private int vidas;
	private int puntos;
	private boolean booObst; 
	private int contObst;
	private boolean booSold; 
	private int contSold;
	private int contRandom;
	private boolean estaSubiendo;
	private boolean estaBajando;
	private boolean jugando;
	private Image fondo;
	private Image princesa;
	private Image soldado;
	private Image gato;
	private boolean cartel;
	private int contcartel;

	private LinkedList <Obstaculo> listaObst;
	
	private LinkedList <Soldado> listaSold;
	
	private LinkedList <BolaDeFuego> listaBolas = new LinkedList <BolaDeFuego> ();
	
	private LinkedList <Moneda> listaMo;

	
	Juego()
	{
		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, "Super Elizabeth Sis - Grupo 9 - Integrantes: Maria Carla Buffagna, Evelyn Bogao y Maximiliano Ali", 800, 600);
		
		// Carga de imagenes
		this.fondo = Herramientas.cargarImagen("Imagen/ImagenDeFondo.png");
		this.princesa = Herramientas.cargarImagen("Imagen/Princesa.png");
		this.soldado = Herramientas.cargarImagen("Imagen/Soldado.png");
		this.gato = Herramientas.cargarImagen("Imagen/Gato.png");

		// Inicializa las variables de instancia
		this.prin = new Princesa(this.princesa, 200, 400, 100,50);
		this.contTick = 0;
		this.vidas = 3;
		this.puntos = 0;
		this.booObst = false;
		this.contObst = 0;
		this.booSold = false;
		this.contSold = 0;
		this.contRandom = 0;
		this.estaSubiendo = false;
		this.estaBajando = false;
		this.jugando = true;
		this.cartel = false;
		this.contcartel = 0;

		
		// Inicializacion de listas y suma de objetos a ellas
		this.listaObst = new LinkedList <Obstaculo> ();
		listaObst.add(new Obstaculo (825,425,40,50,Color.DARK_GRAY));
		listaObst.addLast(new Obstaculo (825,425,40,50,Color.DARK_GRAY));
		listaObst.addLast(new Obstaculo (825,425,40,50,Color.DARK_GRAY));
		listaObst.addLast(new Obstaculo (825,425,40,50,Color.DARK_GRAY));
		listaObst.addLast(new Obstaculo (825,425,40,50,Color.DARK_GRAY));
		
		this.listaSold = new LinkedList <Soldado> ();
		listaSold.add(new Soldado(this.soldado, 1000, 425, 50, 30));
		
		this.listaMo = new LinkedList <Moneda>();	
		
		
		// Inicia el juego!
		this.entorno.iniciar();
		
		
		// Carga e inicio del sonido
		Herramientas.loop("Sonido/SonidoDelJuego.wav");
	}
	
	
	// METODOS DE JUEGO
	
	//funcion que dibuja y mueve los obtaculos
	public void movimientoObst() {
		for (int i =0; i<this.listaObst.size();i++) {
			int bordeDer = this.listaObst.get(i).getX()+ this.listaObst.get(i).getAncho()/2;
			if(i==0) {
				if(bordeDer>0) {
					this.listaObst.get(i).dibujar(this.entorno);
					this.listaObst.get(i).moverIzq();
				}
			}
			if ((i==1 || i==3) && bordeDer> 0&& this.listaObst.get(i-1).getX()+ this.listaObst.get(i-1).getAncho()/2 <600) {
				this.listaObst.get(i).dibujar(this.entorno);
				this.listaObst.get(i).moverIzq();
			}
			if((i==2 || i==4) && bordeDer> 0&& this.listaObst.get(i-1).getX()+ this.listaObst.get(i-1).getAncho()/2 <650) {
				this.listaObst.get(i).dibujar(this.entorno);
				this.listaObst.get(i).moverIzq();
			}
			if (bordeDer == 0) {
				this.listaObst.get(i).setX(825);
				Obstaculo aux = this.listaObst.get(i);
				this.listaObst.remove(i);
				this.listaObst.addLast(aux);
			}
		}
	}
		
	// Colision entre la princesa y el obstaculo  
	public boolean colisionPrinObst(Obstaculo obst) {
		boolean choqueY = this.prin.getY()+ this.prin.getAlto()/2 > obst.getY()- obst.getAlto()/2;
		boolean choqueXinic = this.prin.getX()+this.prin.getAncho()/2 > obst.getX()- obst.getAncho()/2;
		boolean choqueXfin = obst.getX()+ obst.getAncho()/2 > this.prin.getX()-this.prin.getAncho()/2;
		if (choqueY && (choqueXinic && choqueXfin)){
			return true;
			}
		return false;		 
	}
	
	// Colisi�n entre princesa y soldado
	public boolean colisionPrinSold(Soldado sold) {
		boolean choqueY = this.prin.getY()+ this.prin.getAlto()/2 > sold.getY()- sold.getAlto()/2;
		boolean choqueXinic = this.prin.getX()+this.prin.getAncho()/2 > sold.getX()- sold.getAncho()/2;
		boolean choqueXfin = sold.getX()+ sold.getAncho()/2 > this.prin.getX()-this.prin.getAncho()/2;
		if (choqueY && (choqueXinic && choqueXfin)){
			return true;
			}
		return false;		 
	}
	
	
	// Colision entre bola y soldado
	public boolean colisionSoldadoBola(Soldado s, BolaDeFuego bola) {
		boolean choqueXinic = bola.getX()+ bola.getDiametro()/2 > s.getX()- s.getAncho()/2;
		boolean choqueXfin = bola.getX()- bola.getDiametro()/2 < s.getX()+ s.getAncho()/2;
		boolean choqueY = bola.getY()+ bola.getDiametro()/2 > s.getY()- s.getAlto()/2;
		if (choqueXinic && choqueY && choqueXfin){
			return true;
		}	
		return false;	 
	}
	

	// Dibuja y mueve los soldados
	public void movimientoSold() {
		for (int i =0; i<this.listaSold.size();i++) {
			this.listaSold.get(i).dibujar(this.entorno);
			this.listaSold.get(i).moverIzquierda();	
			if (this.listaSold.get(i).getX()+ this.listaSold.get(i).getAncho()/2 <= 0) {
				this.listaSold.remove(i);
			}
		}
	}
	
	// Agrega aleatoriamente soldados
	public void agregarSoldRandom() {
		Random aleatorio = new Random();
		int intAleatorio = aleatorio.nextInt(100);
		if(this.contRandom == intAleatorio) {
			Soldado s = new Soldado(this.soldado,825,425,50,30);
			this.listaSold.add(s);
			
		}
	}
	
	// Agrega aleatoriamente monedas
	public void agregarMonedaRandom() {
		if (this.listaMo.size()<3) {
			Random aleatorio = new Random();
			int hMoneda = aleatorio.nextInt(230)+ 200;
			Random aleatorio1 = new Random ();
			int alea = aleatorio1.nextInt(100);
			if(this.contRandom == alea )
				this.listaMo.add(new Moneda(800,hMoneda));
		}
	}
	
	
	// Dibujo, mueve y elimina por fuera de pantalla Monedas
	public void movimientoMoneda() {
		LinkedList <Moneda> limMoneda = new LinkedList <Moneda>();
		for (Moneda m : this.listaMo) {
			m.dibujar(this.entorno);
			m.mover();
			if(m.getX() <= 0) {
				limMoneda.add(m);
			}
		}	
		this.listaMo.removeAll(limMoneda);
	}
	
	
	// Colision entre princesa y moneda
	public boolean colisionPrinMoneda(Moneda m) {
		boolean choqueXinic = m.getX()- m.getDiametro()/2 < this.prin.getX()+ this.prin.getAncho()/2;
		boolean choqueXfin = m.getX()+ m.getDiametro()/2 > this.prin.getX()- this.prin.getAncho()/2;
		boolean choqueYinic = m.getY()+ m.getDiametro()/2 > this.prin.getY()- this.prin.getAlto()/2;
		boolean choqueYfin = m.getY()- m.getDiametro()/2 < this.prin.getY()+ this.prin.getAlto()/2;
		if (choqueXinic && choqueXfin && choqueYinic && choqueYfin){
			return true;
		}				
		return false;	 
	}
	
	
	// Resta las vidas de la princesa
	public void restarVidas() {
		if(this.vidas==3 ) {
			this.vidas = 2;
		} else if(this.vidas == 2 ) {
			this.vidas = 1;
		} else if(this.vidas == 1) {
			this.vidas = 0;
		}else if (this.vidas == 0) {
			this.vidas = -1;
		}
	}
	
	// Escribe en pantalla que la princesa perdio una vida
	public void cartelPerdioVida() {
		if(this.cartel && this.contcartel + 20 >= this.contTick) {
			String perdioVida= " -1 vida";
			int posX = this.prin.getX();
			int posY = this.prin.getY()- this.prin.getAlto()- 5;
			this.entorno.cambiarFont(perdioVida, 30, Color.RED);
			this.entorno.escribirTexto(perdioVida,posX, posY);
			this.cartel = true; 
			}else {
				this.cartel = false;
			}
	}
	
	
	// Funcion que dibuja en pantalla mensaje de juego terminado
	public void mensajeFinal() {
		if(this.puntos == 100 || this.contTick == 2250) {
			String gano = "FELICIDADES..!!"; 
			String gano1= "La princesa Elizabeth ha rescatado al gatito Carlos";
			String gano2= "Su puntaje fue de:" + this.puntos;
			
			//Imagen de gato
			this.entorno.dibujarImagen(this.gato, 700, 450, 0, 1);
			
			this.entorno.cambiarFont(gano, 30, Color.RED);
			this.entorno.escribirTexto(gano,300, 285);
			this.entorno.cambiarFont(gano1, 25, Color.RED);
			this.entorno.escribirTexto(gano1,100, 330);
			this.entorno.cambiarFont(gano2, 20, Color.RED);
			this.entorno.escribirTexto(gano2,300, 370);
			
		}
		else if(this.vidas == -1){
			String perdio = "Lo siento";
			String perdio1= "La princesa Elizabeth no ha podido rescatar al gatito Carlos";
			String perdio2 = "Su puntaje fue de:" + this.puntos;
			
			this.entorno.cambiarFont(perdio, 30, Color.RED);
			this.entorno.escribirTexto(perdio, 320, 285);
			this.entorno.cambiarFont(perdio1, 25, Color.RED);
			this.entorno.escribirTexto(perdio1, 80, 330);
			this.entorno.cambiarFont(perdio2, 20, Color.RED);
			this.entorno.escribirTexto(perdio2, 300, 370);
		}
	}

	
	/**
	 * Durante el juego, el metodo tick() sera ejecutado en cada instante y 
	 * por lo tanto es el metodo mas importante de esta clase. Aqui se debe 
	 * actualizar el estado interno del juego para simular el paso del tiempo . 
	 */
	
	public void tick()
	{
		// Procesamiento de un instante de tiempo
	
		// Imagen de Fondo
		this.entorno.dibujarImagen(this.fondo, 400, 300, 0, 1);
	
				
		if (jugando) {
			
			// Escribe en pantalla cantidad de vidas y puntos
			String textoVidas = "Vidas : ";
			this.entorno.cambiarFont(textoVidas, 20, Color.BLACK);
			this.entorno.escribirTexto(textoVidas + this.vidas, 650, 50);
			
			String textoPuntos = "Puntaje : ";
			this.entorno.cambiarFont(textoPuntos, 20, Color.BLACK);
			this.entorno.escribirTexto(textoPuntos + this.puntos, 650, 100);
			
			// Por cada tick aumentan los contadores
			this.contTick ++;
			this.contRandom++;
			
			
			//PRINCESA ---------------------------------------------
				
			// Dibujo
			prin.dibujar(this.entorno);
			
			
			// Movimiento izq-der
			if(this.entorno.estaPresionada(this.entorno.TECLA_DERECHA) && prin.getX()<this.entorno.ancho()/2-30) {
				prin.moverDer();
			}
			if(this.entorno.estaPresionada(this.entorno.TECLA_IZQUIERDA)&& prin.getX()>30) {
				prin.moverIzq();
			}
			
			
			// Salto de la princesa
			if(this.entorno.sePresiono(this.entorno.TECLA_ARRIBA) && this.prin.getY()==400 && !estaSubiendo && !estaBajando) {
				estaSubiendo = true;
				this.prin.saltar();
			}
			
			if(estaSubiendo && this.prin.getY()> 200 )
				this.prin.saltar();
	
			if(estaSubiendo && this.prin.getY() <= 200) {
				estaSubiendo = false; 
				estaBajando= true;
			}
	
			if(estaBajando && this.prin.getY() < 400)
				this.prin.caer();
	
		   if(estaBajando && this.prin.getY() >= 399)
			   estaBajando=false;
			
			
		   // OBSTACULOS -------------------------------------------
			
			     // Movimiento y dibujo
			movimientoObst();
			
		   
		  // SOLDADO ---------------------------------------------------
			
				// Movimiento y dibujo
			movimientoSold();	
			
					
		  // BOLA DE FUEGO -----------------------------------------------
			
			// Creacion de bola de fuego
			if(this.entorno.sePresiono(this.entorno.TECLA_ESPACIO) && this.listaBolas.size()<10) {
				this.listaBolas.add(this.prin.crearBolaFuego());
			}			
			 
			// Dibujo, movimiento y eliminacion por fuera de pantalla
			LinkedList <BolaDeFuego> limBola = new LinkedList <BolaDeFuego>();
			
			for (BolaDeFuego b : listaBolas) {
				b.dibujar(this.entorno);
				b.mover();
	
				if(b.getX() >= 800 - (b.getDiametro()/2)) {
					limBola.add(b);
				}
			}	
			this.listaBolas.removeAll(limBola);
			
			
			//MONEDA
			agregarMonedaRandom();
			movimientoMoneda();
			
			
			// COLISIONES ---------------------------------------------------------	
						
			// Colision entre princesa y obstaculo 
			for (Obstaculo obs: this.listaObst) {
				if(colisionPrinObst(obs)&& this.booObst == false) {
					restarVidas(); 
					this.booObst = true; 
					this.contObst = this.contTick;
					this.cartel = true;
					this.contcartel = this.contTick;
				}
			}
			
			while (this.booObst == true && this.contObst + 91 == this.contTick) {
				this.booObst = false;
			}
		
			
			// Colision entre princesa y soldado
			for (Soldado s: this.listaSold) {
				if(colisionPrinSold(s)&& this.booSold == false) {
					restarVidas();
					this.booSold = true; 
					this.contSold = this.contTick;
					this.cartel = true;
					this.contcartel = this.contTick;
				}
			}
			
			while (this.booSold == true && this.contSold + 81 == this.contTick) {
				this.booSold = false;
			}
			
			
			// Muestra en pantalla en caso de colision Princesa-soldado o Princesa-obstaculo
			cartelPerdioVida();
			
			
			// Colisi�n entre Soldado y Bola de Fuego
			LinkedList <BolaDeFuego> bolasARemover = new LinkedList<BolaDeFuego>();
			LinkedList <Soldado> soldadosARemover = new LinkedList<Soldado>();
			
			for (BolaDeFuego b : listaBolas) {
				for(Soldado s : listaSold) {				
					if(colisionSoldadoBola(s,b)) {
						// Carga e inicio del sonido
						Herramientas.cargarSonido("Sonido/Bomba.wav");
						Herramientas.play("Sonido/Bomba.wav");
						bolasARemover.add(b);
						soldadosARemover.add(s);
						this.contRandom = 1;
						this.puntos = this.puntos+5;
					}
				}
			}
			
			listaBolas.removeAll(bolasARemover);
			listaSold.removeAll(soldadosARemover);
			
			if(this.contRandom == 250)
				this.contRandom = 1;
			
			if(this.listaSold.size()<4) {
				agregarSoldRandom();
			}
			
			
			// colision entre princesa y moneda
			LinkedList <Moneda> MonedaARemover = new LinkedList <Moneda>();
			
			for (Moneda m: this.listaMo) {
				if(colisionPrinMoneda(m)) {
					MonedaARemover.add(m);
					this.puntos = this.puntos+10;
					}
			}
			this.listaMo.removeAll(MonedaARemover);
		
			
			
			// Condiciones por las que termina eljuego
			if(this.puntos == 100 || this.contTick == 2250 || this.vidas == -1 )
				jugando = false;
		}
		
		
		// si jugando = false el juego se da por terminado 
		
		// Lanzamos un cartel en consola que indica si gano o perdio
		mensajeFinal();
	}	
	
	
			
	@SuppressWarnings("unused")
	public static void main(String[] args)
	{
		Juego juego = new Juego();
	}
}
